

#include "profiler.h"


inline void InitializeProfiler( struct Profiler* profiler )
{
    profiler->startTime = 0;
    profiler->stopTime = 0;
    profiler->timeTaken = 0;
}

void StartProfiler( struct Profiler* profiler )
{
    profiler->timeTaken = 0.0;
    profiler->stopTime = 0;
    profiler->startTime = clock();
}

void StopProfiler( struct Profiler* profiler )
{
    if( !profiler->startTime )
        return;

    profiler->stopTime = clock();
    profiler->timeTaken = ( (double) profiler->stopTime - profiler->startTime ) / CLOCKS_PER_SEC;
}

void PrintProfilerTimeTakenForActivity( struct Profiler* profiler, 
                                        const char* activity )
{
    // In seconds
    printf("Time taken to do %s: %f seconds\n", activity, profiler->timeTaken);
}

void PrintProfilerTimeTakenForActivityToFile( FILE* fp, struct Profiler* profiler, 
                                              const char* activity )
{
    // In seconds
    fprintf(fp, "Time taken to do %s: %f seconds\n", activity, profiler->timeTaken);
}


