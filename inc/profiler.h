
#ifndef PROFILER_H
#define PROFILER_H

// Since this code is compiled with gcc (not g++), I must specify this code is C code, not C++ code. This allows g++ to link to this library.
#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <time.h>


struct Profiler
{
    clock_t startTime, stopTime;
    double timeTaken;
};

void InitializeProfiler( struct Profiler* profiler );
void StartProfiler( struct Profiler* profiler );
void StopProfiler( struct Profiler* profiler );
void PrintProfilerTimeTakenForActivity( struct Profiler* profiler, 
                                        const char* activity );
void PrintProfilerTimeTakenForActivityToFile( FILE* fp, struct Profiler* profiler, 
                                              const char* activity );


#ifdef __cplusplus 
}
#endif

#endif // PROFILER_H
