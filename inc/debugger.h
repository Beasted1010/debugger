
#ifndef DEBUGGER_H
#define DEBUGGER_H

/*
 * IDEAs: 
 * 	Real-time hot-swap-like debugging (code changes reflected immeidately)
 * 	Monitoring of System Metrics (e.g. how fast things comin in to system, coming out of system (e.g. resources), how many things in middle (e.g. in processing), how many processed so far
*/

// Since this code is compiled with gcc (not g++), I must specify this code is C code, not C++ code. This allows g++ to link to this library.
//   This seems to be due to linkage with C++, due to C++ supporting overriding each function gets a unique symbol.
//       So the function g++ will be looking for would be of a different name to ensure each function has a unique symbol
//       This different name might look like _Z1hic for a void h(int, char) function
#ifdef __cplusplus 
extern "C" {
#endif

#include <stdio.h>
#include <time.h>

#include "profiler.h"


struct Timer
{

};

struct Memory
{

};

struct Debugger
{
    struct Profiler profiler;
};

void CreateDebugger( struct Debugger* debuggerOut );
void DestroyDebugger( struct Debugger* debugger );


#ifdef __cplusplus 
}
#endif

#endif // DEBUGGER_H
