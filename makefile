
INC=inc
SRC=src
OBJ=obj
LIB=lib

MK_LIB=ar rcs
LIB_NAME=fyedebugger.a

CC=gcc
CFLAGS=-Wall -I$(INC)


_DEPS=profiler.h debugger.h

DEPS=$(patsubst %, $(INC)/%, $(_DEPS))
SOURCES=$(subst .h,.c, $(patsubst %, $(SRC)/%, $(_DEPS)))
OBJECTS=$(subst .h,.o, $(patsubst %, $(OBJ)/%, $(_DEPS)))


$(OBJ)/%.o: $(SRC)/%.c $(DEPS)
	if not exist "$(OBJ)" mkdir $(OBJ)
	$(CC) -c -o $@ $< $(CFLAGS)


library: $(SOURCES) $(OBJECTS) $(DEPS)
	$(MK_LIB) $(LIB)/lib$(LIB_NAME) $(OBJECTS)


.PHONY: help
help:
	$(info $$SOURCES = [${SOURCES}])
	$(info $$OBJECTS = [${OBJECTS}])

.PHONY: clean
clean:
ifndef OBJ
	$(error OBJ is not set)
else
	del /q $(OBJ)\*
endif




